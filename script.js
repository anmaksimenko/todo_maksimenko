(function () {
  const inputMessage = document.querySelector('.input__text');
  const button = document.querySelector('.add-todo');
  const list = document.querySelector('.ul-list');
  const checkedAll = document.querySelector('.all-check');
  const selectAllTask = document.querySelector('#all');
  const selectActiveTask = document.querySelector('#act');
  const selectCompletedTask = document.querySelector('#complete');
  const completedCount = document.querySelector('#complete');
  const allCount = document.querySelector('#all');
  const activeCount = document.querySelector('#act');
  const paginationButton = document.querySelector('#pag');
  const buttonDeleteAll = document.querySelector('#btn');
  const { _ } = window;
  let listTodo = [];
  const listeners = {};
  let page = 1;
  const taskOnPage = 5;
  const enterKey = 'Enter';
  const escapeKey = 'Escape';
  let filter = 'all';
  selectAllTask.classList.add('tab-click');
  if (localStorage.listTodo) {
    listTodo = JSON.parse(localStorage.getItem('listTodo'));
  }
  const addListeners = (selector, listener) => {
    const elements = document.querySelectorAll(selector);
    elements.forEach((element) => {
      element.addEventListener('click', listener);
    });
  };
  const removeListeners = (selector, listener) => {
    const elements = document.querySelectorAll(selector);
    elements.forEach((element) => {
      element.removeEventListener('click', listener);
    });
  };
  const addListenersEdit = () => {
    const edits = document.querySelectorAll('.el-2');
    edits.forEach((edit) => {
      edit.addEventListener('dblclick', listeners.startEdit, true);
    });
  };
  const removeListenersEdit = () => {
    const edits = document.querySelectorAll('.el-2');
    edits.forEach((edit) => {
      edit.removeEventListener('dblclick', listeners.startEdit, true);
    });
  };
  const renderPagination = (array) => {
    paginationButton.innerHTML = '';
    const numberPages = Math.ceil(array.length / taskOnPage);
    const renderString = Array.from(
      { length: numberPages },
      (value, i) => i + 1,
    ).reduce((string, item) => (
      `${string}
      <li class="block-column page-item page-list ${item === page ? 'active' : ''
      }"><span id="page-${item}" class="page-link">${item}</span></li>`), '');
    paginationButton.innerHTML = renderString;
  };
  const getTask = () => {
    if (filter === 'completed') {
      return listTodo.filter((item) => item.check);
    }
    if (filter === 'active') {
      return listTodo.filter((item) => !item.check);
    }
    return listTodo;
  };
  const updateData = () => {
    localStorage.setItem('listTodo', JSON.stringify(listTodo));
  };
  const countTask = () => {
    const completed = listTodo.filter((item) => item.check);
    completedCount.innerHTML = `Completed (${completed.length})`;
    const active = listTodo.filter((item) => !item.check);
    activeCount.innerHTML = `Active (${active.length})`;
    allCount.innerHTML = `All (${listTodo.length})`;
  };
  const render = (array) => {
    list.innerHTML = '';
    const start = (page - 1) * taskOnPage;
    const end = start + taskOnPage;
    const renderString = array.slice(start, end).reduce((string, item) => (
      `${string}
    <div class="item-list ${item.check ? 'checked' : ''}">
        <div class="b__row">
          <div class="b__element el-1">
            <div class="b-cont-1">
              <input type="checkbox" class="button-check" id="check-${item.id}" ${item.check ? 'checked' : ''}>
            </div>
          </div>
          <div class="b__element el-2" id="edit-${item.id}">
            <div class="b-cont-2">
              <div class="message">${item.message}</div>
            </div>
          </div>
          <div class="b__element el-3">
            <div class="b-cont-3">
              <button class="button-delete" id="delete-${item.id}">\u00D7</button>
            </div>
          </div>
        </div>
      </div>`), '');
    list.innerHTML = renderString;
    countTask();
  };
  const callRender = () => {
    removeListenersEdit();
    removeListeners('.page-list', listeners.changePage);
    removeListeners('.button-delete', listeners.delList);
    removeListeners('.button-check', listeners.checkList);
    render(getTask());
    renderPagination(getTask());
    addListeners('.page-list', listeners.changePage);
    addListeners('.button-delete', listeners.delList);
    addListeners('.button-check', listeners.checkList);
    addListenersEdit();
  };
  const changePage = (event) => {
    page = Number(event.target.id.slice(5));
    callRender();
  };
  const changeClassTab = (isAddClass, element) => {
    if (isAddClass) {
      element.classList.add('tab-click');
    } else {
      element.classList.remove('tab-click');
    }
  };
  const changeTab = (tab) => {
    filter = tab;
    page = 1;
    changeClassTab(tab === 'all', selectAllTask);
    changeClassTab(tab === 'active', selectActiveTask);
    changeClassTab(tab === 'completed', selectCompletedTask);
    callRender();
  };
  const actualPage = (array) => {
    const numberPages = Math.ceil(array.length / taskOnPage);
    if (page > numberPages && numberPages !== 0) {
      page = numberPages;
    }
    if (numberPages === 0) {
      page = 1;
    }
  };
  const checkList = (event) => {
    const id = Number(event.target.id.slice(6));
    listTodo = listTodo.map((item) => {      
      if (item.id === id) {
        return {
          ...item,
          check: !item.check,
        };
      }
      return item;
    });
    if (checkedAll.checked || listTodo.every((item) => {return item.check})){
      checkedAll.checked = !checkedAll.checked;
    }
    updateData();
    actualPage(getTask());
    callRender();
  };
  const checkListAll = () => {
    const checkedAllStat = checkedAll.checked;
    listTodo = listTodo.map((item) => ({
      ...item,
      check: checkedAllStat,
    }));
    updateData();
    actualPage(getTask());
    callRender();
  };
  const delList = (event) => {
    const id = Number(event.target.id.slice(7));
    listTodo = listTodo.filter((item) => item.id !== id);
    updateData();
    actualPage(getTask());
    callRender();
  };
  const delListsCompleted = () => {
    listTodo = listTodo.filter((item) => !item.check);
    if (checkedAll.checked) {
      checkedAll.checked = !checkedAll.checked;
    }
    updateData();
    actualPage(getTask());
    callRender();
  };
  const goToLastPage = () => {
    page = Math.ceil(listTodo.length / taskOnPage);
    filter = 'all';
  };
  const addTask = () => {
    const text = inputMessage.value.replace(/\s+/g, ' ');    
    if (!inputMessage.value.trim()) {
      inputMessage.value = '';
    }
    const validText = _.escape(text);
    if (!inputMessage.value) return;
    const task = {
      check: false,
      message: validText,
      id: Date.now(),
    };
    listTodo.push(task);
    selectAllTask.classList.add('tab-click');
    selectActiveTask.classList.remove('tab-click');
    selectCompletedTask.classList.remove('tab-click');
    goToLastPage();
    updateData();
    callRender();
    inputMessage.value = '';
  };
  const saveEdit = (event, action, listener) => {
    const input = document.querySelector(`#${event.target.id}`);
    const value = input.value.replace(/\s+/g, ' ');
    if (input.value.trim()) {
      const validText = _.escape(value);
      listTodo = listTodo.map((item) => {
        if (`input-${item.id}` === event.target.id) {
          return {
            ...item,
            message: validText,
          };
        }
        return item;
      });
      updateData();
    }
    actualPage(getTask());
    callRender();
    input.removeEventListener(action, listener);
  };
  const enterEdit = (event) => {
    if (event.key === enterKey) {
      saveEdit(event, 'keyup', enterEdit);
    }
  };
  const blurEdit = (event) => {
    saveEdit(event, 'blur', blurEdit);
  };
  const escapeEdit = (event) => {
    if (event.key === escapeKey) {
      const input = document.querySelector(`#${event.target.id}`);
      actualPage(getTask());
      callRender();
      input.removeEventListener('keyup', escapeEdit);
    }
  };
  const startEdit = (event) => {
    const id = Number(event.currentTarget.id.slice(5));
    const value = listTodo.find((item) => item.id === id).message;
    const editElem = document.querySelector(`#edit-${id}`);
    const input = document.createElement('input');
    input.classList.add('style-edit');
    input.id = `input-${id}`;
    input.value = _.unescape(value.replace(/\s+/g, ' '));
    editElem.innerHTML = '';
    editElem.append(input);
    input.focus();
    input.addEventListener('keyup', enterEdit);
    input.addEventListener('blur', blurEdit);
    input.addEventListener('keyup', escapeEdit);
  };
  button.addEventListener('click', addTask);
  inputMessage.addEventListener('keyup', (ev) => {
    if (ev.key === enterKey) {
      addTask();
    }
  });
  inputMessage.addEventListener('keyup', (ev) => {
    if (ev.key === enterKey) {
      inputMessage.value = '';
    }
  });
  checkedAll.addEventListener('click', checkListAll);
  buttonDeleteAll.addEventListener('click', delListsCompleted);
  selectAllTask.addEventListener('click', () => changeTab('all'));
  selectActiveTask.addEventListener('click', () => changeTab('active'));
  selectCompletedTask.addEventListener('click', () => changeTab('completed'));
  listeners.changePage = changePage;
  listeners.delList = delList;
  listeners.checkList = checkList;
  listeners.startEdit = startEdit;
  render(getTask());
  renderPagination(getTask());
  addListenersEdit();
  addListeners('.page-list', listeners.changePage);
  addListeners('.button-delete', listeners.delList);
  addListeners('.button-check', listeners.checkList);
}());
